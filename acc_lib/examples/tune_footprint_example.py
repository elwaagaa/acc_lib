"""
Example to perform turn-by-turn tracking and plot the tune footprint 
by Elias Waagaard 
"""
import json
import acc_lib
import matplotlib.pyplot as plt
import xpart as xp
import xobjects as xo
import xtrack as xt

# Specify json sequence file - SPS ions 
fname_line = '../SPS_sequence/SPS_2021_Pb_ions_for_tracking.json'

# Set up the context 
context = xo.ContextCpu()
with open(fname_line, 'r') as fid:
     input_data = json.load(fid)

# Load the line and reference particle
line = xt.Line.from_dict(input_data['line'])
particle_ref = xp.Particles.from_dict(input_data['particle'])

tracker = xt.Tracker(_context=context, line=line)
twiss_xtrack = tracker.twiss() 

# INSTALL SPACE CHARGE AND DO TRACKING (TURN-BY-TURN MONITOR ON AND OFF) AND SHOW TUNE DIAGRAM
