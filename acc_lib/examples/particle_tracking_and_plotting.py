"""
Example to track particles and plot phase space ellipse and centroid motion
by Elias Waagaard 
"""
import json
import acc_lib
import numpy as np
import matplotlib.pyplot as plt
import xpart as xp
import xobjects as xo
import xtrack as xt
import xfields as xf

# Specify json sequence file - SPS ions 
fname_line = '../SPS_sequence/SPS_2021_Pb_ions_for_tracking.json'

# Set up the context 
context = xo.ContextCpu()
with open(fname_line, 'r') as fid:
     input_data = json.load(fid)

# Load the line and reference particle
line = xt.Line.from_dict(input_data['line'])
particle_ref = xp.Particles.from_dict(input_data['particle'])

# Beam settings
num_turns = 30
num_particles = 1000
bunch_intensity = 3.5e8 
sigma_z = 22.5e-2 
nemitt_x= 1.3e-6*(particle_ref.gamma0[0]*particle_ref.beta0[0])   
nemitt_y= 0.9e-6*(particle_ref.gamma0[0]*particle_ref.beta0[0])  
delta0 = 0.0
num_spacecharge_interactions = 540   # number of SC kicks per turn 
tol_spacecharge_position = 1e-2   # tolerance in meters by how much location at which space charge kick is given can move 

# Install frozen space charge
lprofile = xf.LongitudinalProfileQGaussian(
        number_of_particles=bunch_intensity,
        sigma_z=sigma_z,
        z0=0.,
        q_parameter=1.)

xf.install_spacecharge_frozen(line=line,
                   particle_ref=particle_ref,
                   longitudinal_profile=lprofile,
                   nemitt_x=nemitt_x, nemitt_y=nemitt_y,
                   sigma_z=sigma_z,
                   num_spacecharge_interactions=num_spacecharge_interactions,
                   tol_spacecharge_position=tol_spacecharge_position)

# Build tracker
tracker = xt.Tracker(_context=context, line=line)

# Generate matched beam of particles 
particles = xp.generate_matched_gaussian_bunch(_context=context,
         num_particles=num_particles, total_intensity_particles=bunch_intensity,
         nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
         particle_ref=particle_ref, tracker=tracker)

# Track the particles 
tracker.track(particles, num_turns=num_turns, turn_by_turn_monitor=True)

# Plot the centroid tracking 
fig = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_centroid_motion(fig, tracker)
fig.tight_layout()
fig.savefig("../plots/centroid.png", dpi=250)

# Plot the phase space diagram of the first particle
fig2 = plt.figure(figsize=(10,7))
acc_lib.plot_tools.plot_phase_space_ellipse(fig2, tracker)
fig2.savefig("../plots/phase_space_ellipse.png", dpi=250)

# ------------- Plot tune diagram with resonance lines ----------------
Qh_set = 26.30
Qv_set = 26.25
Qx_int_set = 26
Q_interval = 1e-1 # tune diagram interval to plot
  
fig3 = plt.figure(figsize=(10,7))
tune_diagram = acc_lib.resonance_lines([Qh_set-Q_interval,Qh_set+Q_interval],[Qv_set-Q_interval,Qv_set+Q_interval], 1+np.arange(4), 16)
Qx_min, Qx_max, Qy_min, Qy_max, axFP = tune_diagram.plot_resonance_and_tune_footprint(tracker, Qx_int_set, fig3)
axFP.plot(Qh_set, Qv_set, 'ro', markersize=14, label="Set tune")
axFP.legend(loc='upper left', fontsize=14)
fig3.tight_layout()
fig3.savefig("../plots/tune_footprint_and_resonances.png", dpi=250)