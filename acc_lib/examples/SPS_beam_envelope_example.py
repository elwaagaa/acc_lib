"""
Example to plot SPS sequence beam envelope for Q20 optics
by Elias Waagaard 
"""
import matplotlib.pyplot as plt
from cpymad.madx import Madx
from cpymad import libmadx
import os

import acc_lib

# Initiate cpymad sequence
with open('tempfile', 'w') as f:
    madx = Madx(stdout=f,stderr=f)  

#Activate the aperture for the Twiss flag to include it in Twiss command! 
madx.option(echo=False, warn=True, info=False, debug=False, verbose=False)
madx.input('select,flag=twiss,clear;')
madx.input('SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;')

"""
# Load the sequence 
seq_name = 'sps'
optics = "/afs/cern.ch/eng/acc-models/sps/2021"
if not os.path.isdir("sps"):
    os.symlink(optics, "sps")


madx.input('RBARC=false;')
madx.call("sps/beams/beam_lhc_injection.madx", chdir=True)
madx.call("sps/scenarios/lhc/lhc_q20/job.madx")
"""


# Call the SPS sequence and the aperture files
madx.call('../SPS_sequence/SPS_2021_LHC_q20_thick_test.seq')
madx.call("../SPS_sequence/sps_aperture_data/aperturedb_1.dbx")
madx.call("../SPS_sequence/sps_aperture_data/aperturedb_2.dbx")
#madx.call("../SPS_sequence/sps_aperture_data/aperturedb_3.dbx") 
# For aperture dbx 3, why do we get "fatal: negative drift between elements  \
 #       ap.do.zs21633:1 and zs.21633:1, length -1.565000e+00"? 

# Perform Twiss command 
madx.use(sequence='sps')
twiss = madx.twiss()

# Plot the envelope
fig = plt.figure(figsize=(10,7))
ax = acc_lib.madx_tools.plot_envelope(fig, madx, twiss, seq_name='sps', axis='horizontal')
aperture_position, aperture = acc_lib.madx_tools.get_apertures_real(twiss, axis='horizontal')
acc_lib.madx_tools.plot_apertures_real(ax, aperture_position, aperture) 
